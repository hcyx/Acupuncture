﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class point : MonoBehaviour {

    public string name;

    public string info;

    protected AudioSource source;
    

	// Use this for initialization
	void Start () {
        source = this.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag.CompareTo("finger") == 0)
        {
            GameObject.Find("Canvas/Name").GetComponent<Text>().text = name;
            GameObject.Find("Canvas/Info").GetComponent<Text>().text = info;
            source.Play();
        }
    }
}
