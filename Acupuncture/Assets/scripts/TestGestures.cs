﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class TestGestures : MonoBehaviour {
	public float rotateFactor;
	public enum RotationMethod
	{
		None,
		Single,
		Full
	}

	[SerializeField]
	private PinchDetector _pinchDetectorA;
	public PinchDetector PinchDetectorA
	{
		get
		{
			return _pinchDetectorA;
		}
		set
		{
			_pinchDetectorA = value;
		}
	}

	[SerializeField]
	private PinchDetector _pinchDetectorB;
	public PinchDetector PinchDetectorB
	{
		get
		{
			return _pinchDetectorB;
		}
		set
		{
			_pinchDetectorB = value;
		}
	}

	[SerializeField]
	private RotationMethod _oneHandedRotationMethod;

	[SerializeField]
	private RotationMethod _twoHandedRotationMethod;

	[SerializeField]
	private bool _allowScale = true;


	private Transform _anchor;

	private float _defaultNearClip;

	void Start()
	{
		//      if (_pinchDetectorA == null || _pinchDetectorB == null) {
		//        Debug.LogWarning("Both Pinch Detectors of the LeapRTS component must be assigned. This component has been disabled.");
		//        enabled = false;
		//      }

		GameObject pinchControl = new GameObject("baby");
		_anchor = pinchControl.transform;
		_anchor.transform.parent = transform.parent;
		transform.parent = _anchor;
	}

	void Update()
	{

		bool didUpdate = false;
		if (_pinchDetectorA != null)
			didUpdate |= _pinchDetectorA.DidChangeFromLastFrame;
		if (_pinchDetectorB != null)
			didUpdate |= _pinchDetectorB.DidChangeFromLastFrame;

		if (didUpdate)
		{
			transform.SetParent(null, true);
		}

		if (_pinchDetectorA != null && _pinchDetectorA.IsPinching &&
			_pinchDetectorB != null && _pinchDetectorB.IsPinching)
		{
			transformDoubleAnchor();
		}
		else if (_pinchDetectorA != null && _pinchDetectorA.IsPinching)
		{
			transformSingleAnchor(_pinchDetectorA);
		}
		else if (_pinchDetectorB != null && _pinchDetectorB.IsPinching)
		{
			transformSingleAnchor(_pinchDetectorB);
		}

		if (didUpdate)
		{
			transform.SetParent(_anchor, true);
		}
	}

	void OnGUI()
	{

	}



	private void transformDoubleAnchor()
	{
		/*_anchor.position = (_pinchDetectorA.Position + _pinchDetectorB.Position) / 2.0f;

		switch (_twoHandedRotationMethod)
		{
		case RotationMethod.None:
			break;
		case RotationMethod.Single:
			Vector3 p = _pinchDetectorA.Position;
			p.y = _anchor.position.y;
			_anchor.LookAt(p);
			break;
		case RotationMethod.Full:
			Quaternion pp = Quaternion.Lerp(_pinchDetectorA.Rotation, _pinchDetectorB.Rotation, 0.5f);
			Vector3 u = pp * Vector3.up;
			_anchor.LookAt(_pinchDetectorA.Position, u);
			break;
		}*/

		if (_allowScale)
		{
			_anchor.localScale = Vector3.one * Vector3.Distance(_pinchDetectorA.Position, _pinchDetectorB.Position);
		}
	}

	private void transformSingleAnchor(PinchDetector singlePinch)
	{
		_anchor.position = singlePinch.Position;

		switch (_oneHandedRotationMethod)
		{
		case RotationMethod.None:
			break;
		case RotationMethod.Single:
			Vector3 p = singlePinch.Rotation * Vector3.right;
			p.y = _anchor.position.y;
			p.x *= rotateFactor;
			_anchor.LookAt(p);
			break;
		case RotationMethod.Full:
			_anchor.rotation = singlePinch.Rotation;
			break;
		}

		_anchor.localScale = Vector3.one;
	}
}
